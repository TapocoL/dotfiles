# Lines configured by zsh-newuser-install
HISTFILE=~/.zsh_history
HISTSIZE=10000
SAVEHIST=10000
unsetopt autocd beep correct
setopt histignoredups
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/cjackson/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

# https://wiki.archlinux.org/index.php/Zsh#Command_completion
zstyle ':completion:*' menu select

# https://wiki.archlinux.org/index.php/Zsh#History_search
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "$key[Up]" ]] && bindkey -- "$key[Up]" up-line-or-beginning-search
[[ -n "$key[Down]" ]] && bindkey -- "$key[Down]" down-line-or-beginning-search

# https://wiki.archlinux.org/index.php/Zsh#Prompt_themes
autoload -Uz promptinit
promptinit
prompt redhat

# my prompt
autoload vcs_info
setopt prompt_subst
git_prompt () {
  ref=$(command git symbolic-ref HEAD --short 2> /dev/null)
  if [[ -z $ref ]] {
    ref=$(command git rev-parse --short HEAD 2> /dev/null)
  }
  if [[ -n $ref ]] {
    color="%F{green}"
    dirty=''
    lstatus=$(command git status --porcelain)
    if [[ -n $lstatus ]] {
      dirty="*"
      dirtym=$(command echo "$lstatus" | grep "^ M" | wc -l 2> /dev/null)
      if [[ $dirtym -gt 0 ]] {
        dirty="$dirty %F{blue}%B±$dirtym%b$color"
      }
      dirtya=$(command echo "$lstatus" | grep "^??" | wc -l 2> /dev/null)
      if [[ $dirtya -gt 0 ]] {
        dirty="$dirty %F{green}%B+$dirtya%b$color"
      }
      dirtyd=$(command echo "$lstatus" | grep "^ D" | wc -l 2> /dev/null)
      if [[ $dirtyd -gt 0 ]] {
        dirty="$dirty %F{red}%B-$dirtyd%b$color"
      }
    }
    echo "$color‹$ref$dirty›%f"
  }
}
PROMPT="%(?..%K{red}    %k %F{red}ret %?
%f)%F{blue}- %n@%m%f %F{yellow}%~%f "'$(git_prompt)'"
%F{blue}--- %(#.#.$)%f "

# git aliases
alias gst='git status'
alias gb='git branch'
alias gp='git push'
alias gpro='git pull --rebase origin'
alias gco='git checkout'
alias gr='git remote'
alias ga='git add'
alias gd='git diff'

# my bindkeys
bindkey "^[[1;5C" forward-word
bindkey "^[[1;5D" backward-word
bindkey "^[[3~"   delete-char
bindkey "^[[H"    beginning-of-line
bindkey "^[[F"    end-of-line

#title
#precmd () { print -Pn "\e]0;$TITLE\a" }
#title() { export TITLE="$*" }
chpwd() {
  [[ -t 1 ]] || return
  case $TERM in
    sun-cmd) print -Pn "\e]l%~\e\\"
      ;;
    *xterm*|rxvt|(dt|k|E)term) print -Pn "\e]2;%~\a"
      ;;
  esac
}
chpwd
source /usr/share/nvm/init-nvm.sh
