export EDITOR=/usr/bin/nvim
#export GOROOT=/usr/lib/go
export GOPATH=$HOME/go
export GOBIN=$GOPATH/bin
export ANDROID_HOME=$HOME/AndroidStudio
export PATH=$PATH:$GOBIN:$ANDROID_HOME/platform-tools:$HOME/.rbenv/shims:$HOME/.yarn/bin
export PAGER=/usr/bin/less

