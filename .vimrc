set number
syntax on
set expandtab
set ts=2
set shiftwidth=2
set softtabstop=2
set showmode
set showcmd
set ruler<
set autoindent
set smartindent
set hlsearch
set incsearch
set showmatch
set spelllang=en_us
set nocompatible
set nobackup
set lz
set encoding=utf-8
set wildmenu
set directory=$HOME/.vim/swapfiles//

call plug#begin('~/.vim/plugged')
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree', { 'on': 'NERDTreeToggle' }
" Plug 'cloudhead/neovim-fuzzy' commented until nannery is merged
Plug 'nannery/neovim-fuzzy'
Plug 'fatih/vim-go', { 'tag': 'v1.28', 'do': ':GoUpdateBinaries' }
Plug 'mileszs/ack.vim'
Plug 'SirVer/ultisnips'
Plug 'craigjackson/onyx'
Plug 'godlygeek/tabular'
Plug 'bronson/vim-visual-star-search'
Plug 'cespare/vim-toml', { 'branch': 'main' }
Plug 'rust-lang/rust.vim'
Plug 'dense-analysis/ale', { 'tag': 'v3.2.0' }
Plug 'Shougo/deoplete.nvim'
Plug 'zchee/deoplete-go'
Plug 'leafgarland/typescript-vim'
Plug 'peitalin/vim-jsx-typescript'
Plug 'pangloss/vim-javascript'
Plug 'maxmellon/vim-jsx-pretty'
Plug 'jwalton512/vim-blade'
Plug 'othree/html5.vim'
Plug 'evanleck/vim-svelte', {'branch': 'main'}
Plug 'OmniSharp/omnisharp-vim'
" Plug 'Exafunction/codeium.vim', { 'branch': 'main' }
" Plug 'nomnivore/ollama.nvim', { 'branch': 'main' }
" Plug 'David-Kunz/gen.nvim', { 'branch': 'main' }
call plug#end()

syntax enable
filetype plugin indent on
set mouse=a
set cursorline
let NERDTreeIgnore = ['\.pyc$']
if exists('+colorcolumn')
  set colorcolumn=80
else
  au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

let g:ale_lint_on_text_changed = 'never'
let g:ale_lint_on_insert_leave = 1

let g:ackprg = 'ag --vimgrep --smart-case --ignore node_modules --ignore .git --ignore vendor --ignore tmp --ignore app/cache'
cnoreabbrev ag Ack
cnoreabbrev aG Ack
cnoreabbrev Ag Ack
cnoreabbrev AG Ack

let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<tab>"
let g:UltiSnipsJumpBackwardTrigger="<s-tab>"

if has('nvim')
  let g:deoplete#enable_at_startup = 1
endif

let g:go_highlight_build_constraints = 1
let g:go_highlight_extra_types = 1
let g:go_highlight_fields = 1
let g:go_highlight_functions = 1
let g:go_highlight_methods = 1
let g:go_highlight_operators = 1
let g:go_highlight_structs = 1
let g:go_highlight_types = 1
au Filetype go noremap <leader>gd :GoDef<CR>
au Filetype go noremap <leader>gs :GoSameIds<CR>
au Filetype go noremap <leader>gc :GoSameIdsClear<CR>
au Filetype go noremap <leader>gt :GoInfo<CR>

nnoremap <C-S> :w<CR>
au FileType go setl noexpandtab
au FileType jsx setl noexpandtab
au FileType html setl expandtab
au FileType sql setl expandtab
au FileType tf setl noexpandtab

" let g:codeium_manual = v:true
