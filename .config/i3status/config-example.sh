disk_0_used=$(df -h | sed -n -E "s/^\/dev\/sda2\s+([0-9\.]+)G\s+([0-9\.]+)G\s+[0-9\.]+G\s+([0-9\.]+)%.+$/\2/p")
disk_0_total=$(df -h | sed -n -E "s/^\/dev\/sda2\s+([0-9\.]+)G\s+([0-9\.]+)G\s+[0-9\.]+G\s+([0-9\.]+)%.+$/\1/p")
disk_0_percentage=$(df -h | sed -n -E "s/^\/dev\/sda2\s+([0-9\.]+)G\s+([0-9\.]+)G\s+[0-9\.]+G\s+([0-9\.]+)%.+$/\3/p")
disk_0="$disk_0_used G / $disk_0_total G ($disk_0_percentage%)"

network_interface="enp0s3"
network_0_ip=$(ip addr show $network_interface | sed -n -E "s/^\s+inet ([0-9\.]+).+$/\1/p")
network_0_stat=$(ifstat -t 1 | sed -n -E "s/^$network_interface/\0/p")
network_0_usage=$(echo $network_0_stat | awk '{printf "%5s", $6}')
network_0="$network_0_ip ($network_0_usage)"

battery_status="$(cat /sys/class/power_supply/BAT0/status) $(cat /sys/class/power_supply/BAT0/capacity)%"

uptime=$(uptime)
cpu_load_1=$(echo $uptime | sed -n -E "s/^.*load average: ([0-9\.]+), ([0-9\.]+), ([0-9\.]+)/\1/p")
cpu_load_5=$(echo $uptime | sed -n -E "s/^.*load average: ([0-9\.]+), ([0-9\.]+), ([0-9\.]+)/\2/p")
cpu_load_15=$(echo $uptime | sed -n -E "s/^.*load average: ([0-9\.]+), ([0-9\.]+), ([0-9\.]+)/\3/p")
cpu_sensor="coretemp-isa-0000"
cpu_temp=$(sensors $cpu_sensor | grep "CPU temperature" | sed 's/CPU temperature://' | sed 's/[ +]//g')
cpu_load="$cpu_load_1 $cpu_load_5 $cpu_load_15 ($cpu_temp)"

memory_used=$(expr $(cat /proc/meminfo| sed -n -E "s/^Active:\s+([0-9]+) kB$/\1/p") / 1024)
memory_total=$(expr $(cat /proc/meminfo| sed -n -E "s/^MemTotal:\s+([0-9]+) kB$/\1/p") / 1024)
memory_perc=$(expr $(expr $(cat /proc/meminfo| sed -n -E "s/^Active:\s+([0-9]+) kB$/\1/p") \* 100) / $(cat /proc/meminfo| sed -n -E "s/^MemTotal:\s+([0-9]+) kB$/\1/p"))
memory_usage="$memory_used mB [active] / $memory_total mB ($memory_perc%)"

date_formatted=$(date "+%Y-%m-%d %H:%M:%S %Z")

echo "$disk_0 | $network_0 | $battery_status | $cpu_load | $memory_usage | $date_formatted"
