set runtimepath+=~/.vim,~/.vim/after
set packpath+=~/.vim
source ~/.vimrc

set list
set listchars=tab:▸\ ,eol:¬
set noerrorbells visualbell t_vb=

noremap <c-F> :FuzzyOpen<CR>

" require('gen').setup({model="mistral",host="localhost",port="11434",quit_map="q",retry_map="<c-r>",init = function(options) pcall(io.popen, "ollama serve > /dev/null 2>&1 &") end,display_mode = "float",show_prompt = false,show_model = false,no_auto_close = false,debug = false})

if has("gui_vimr")
" Here goes some VimR specific settings like
  source ~/.config/nvim/ginit.vim
endif
