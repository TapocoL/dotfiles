colorscheme onyx

if exists('g:GtkGuiLoaded')
  set guifont=Fira\ Mono\ 14
  call rpcnotify(1, 'Gui', 'Option', 'Tabline', 0)
  "`NVIM_GTK_NO_HEADERBAR=1 nvim-gtk` to hide header bar
elseif has("gui_vimr")
  GuiFont Fira\ Mono:h14
else "qt
  GuiFont Fira\ Mono:h14
  GuiTabline 0
endif
nnoremap <C-+> :call GuiFont(substitute(g:GuiFont, ':h\zs\d\+', '\=eval(submatch(0)+1)', ''))<CR>
nnoremap <C--> :call GuiFont(substitute(g:GuiFont, ':h\zs\d\+', '\=eval(submatch(0)-1)', ''))<CR>
nnoremap <C-0> :call GuiFont(substitute(g:GuiFont, ':h\zs\d\+', ':h14', ''))<CR>
